package com.sample.simpsonsviewer.characterdetail;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.sample.simpsonsviewer.R;
import com.sample.simpsonsviewer.pojo.Character;

public class CharacterDetailFragment extends Fragment  {

    public static final String ARG_CHARACTER = "character_id";
    private Character character = null;

    public CharacterDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_CHARACTER)) {
            character = getArguments().getParcelable(ARG_CHARACTER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);
        ImageView  characterImage = rootView.findViewById(R.id.characterImage);
        TextView characterDescription = rootView.findViewById(R.id.characterDescription);
        if (character != null) {
            CollapsingToolbarLayout appBarLayout = this.getActivity().findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(character.gettName());
            }
            Glide.with(this).load(character.getIcon().getURL())
                    .placeholder(R.drawable.ic_donut_svgrepo_com)
                    .into(characterImage);
            characterDescription.setText(character.getDescription());
        } else {
            failedToSetupView();
        }

        return rootView;
    }

    public void failedToSetupView() {
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.doh))
                .setMessage(getString(R.string.somethingwentwrong))
                .setNegativeButton(android.R.string.ok, null)
                .setIcon(R.drawable.ic_baseline_error_outline_24px)
                .show();
    }

}
