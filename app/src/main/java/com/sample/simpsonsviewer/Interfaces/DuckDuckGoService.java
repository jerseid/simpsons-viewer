package com.sample.simpsonsviewer.Interfaces;

import com.sample.simpsonsviewer.pojo.RequestData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DuckDuckGoService {
        @GET("./")
        Call<RequestData> getCharacters(@Query("q") String query,
                                                @Query("format") String format);

}
