package com.sample.simpsonsviewer.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Character implements Parcelable {

    @SerializedName("Icon")
    @Expose
    private Icon icon;
    @SerializedName("FirstURL")
    @Expose
    private String firstURL;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("Text")
    @Expose
    private String text;

    private String name;
    private String description;


    protected Character(Parcel in) {
        icon = in.readParcelable(Icon.class.getClassLoader());
        firstURL = in.readString();
        result = in.readString();
        text = in.readString();
        name = in.readString();
        description = in.readString();
    }

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getFirstURL() {
        return firstURL;
    }

    public void setFirstURL(String firstURL) {
        this.firstURL = firstURL;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String gettName(){
        return name;

    }

    public void setName(String name) {
        String[] separated = name.split("-");
        name = separated[0].trim();
        this.name = name;
    }

    public void setDescription(String description) {
        String[] separated = description.split("-");
        description = separated[separated.length -1].trim();
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(icon, flags);
        dest.writeString(firstURL);
        dest.writeString(result);
        dest.writeString(text);
        dest.writeString(name);
        dest.writeString(description);
    }
}