package com.sample.simpsonsviewer.characterlist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.sample.simpsonsviewer.R;
import com.sample.simpsonsviewer.pojo.Character;

import java.util.List;


public class CharacterListActivity extends AppCompatActivity  {

    private boolean mTwoPane;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    CharacterListViewModel model = null;
    private SearchView searchView = null;
    private CharSequence searchQuery = null;
    private static final String QUERYVALUE = "queryValue";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.the_simpsons));
        setSupportActionBar(toolbar);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        model = ViewModelProviders.of(this).get(CharacterListViewModel.class);
        if (savedInstanceState != null && savedInstanceState.containsKey(QUERYVALUE)){
            searchQuery = savedInstanceState.getCharSequence(QUERYVALUE);
        } else {
            showProgress(true);
            getCharactersFromViewModel();
        }

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
        }

    }

    private void setupRecyclerView(List<Character> characters) {
        recyclerView.setAdapter(new CharacterListAdapter(this,characters, mTwoPane));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_with_search, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() > 2) {
                 queryCharacters(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0) {
                    getCharactersFromViewModel();
                    searchView.setIconified(true);
                }
                return false;
            }
        });
        if(searchQuery != null){
            searchView.setQuery(searchQuery, true);
            searchView.setIconified(false);
            searchView.clearFocus();
        }
        return true;
    }

    public void showProgress(boolean isShowingProgress) {
        if (isShowingProgress) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }


    private void failedResponse() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.doh))
                .setMessage(getString(R.string.somethingwentwrong))
                .setNegativeButton(android.R.string.ok, null)
                .setIcon(R.drawable.ic_baseline_error_outline_24px)
                .show();
    }

    private void noResults() {
        Animation wiggle = AnimationUtils.loadAnimation(this, R.anim.wiggle);
        View search = getWindow().getDecorView().findViewById(android.R.id.content).findViewById(R.id.action_search);
        search.startAnimation(wiggle);
    }

    private void getCharactersFromViewModel(){
        model.getCharacters().observe(this, new Observer<List<Character>>() {
            @Override
            public void onChanged(@Nullable List<Character> characters) {
                showProgress(false);
                if (characters != null && characters.size() == 0){
                    failedResponse();
                } else {
                    setupRecyclerView(characters);
                }
            }
        });
    }

    private void queryCharacters(String query){
        model.queryCharacters(query).observe(CharacterListActivity.this, new Observer<List<Character>>() {
            @Override
            public void onChanged(@Nullable List<Character> characters) {
                if (characters != null && characters.size() == 0){
                    noResults();
                } else {
                    setupRecyclerView(characters);
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(searchView.getQuery().length() > 0) {
            outState.putCharSequence(QUERYVALUE, searchView.getQuery());
        }
    }
}
