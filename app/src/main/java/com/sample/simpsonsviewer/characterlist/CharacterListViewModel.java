package com.sample.simpsonsviewer.characterlist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.simpsonsviewer.Interfaces.DuckDuckGoService;
import com.sample.simpsonsviewer.pojo.Character;
import com.sample.simpsonsviewer.pojo.RequestData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CharacterListViewModel extends ViewModel {
    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.duckduckgo.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private DuckDuckGoService service = retrofit.create(DuckDuckGoService.class);

    //This demo's requirements can allow for these parameters to be static
    private static final String SIMPSONSQUERY = "simpsons characters";
    private static final String DATAFORMAT = "json";
    private MutableLiveData<List<Character>> characters;
    public LiveData<List<Character>> getCharacters() {
        if (characters == null) {
            characters = new MutableLiveData<>();
            loadCharacters();
        }
        return characters;
    }

    private void loadCharacters() {

        service.getCharacters(SIMPSONSQUERY, DATAFORMAT).enqueue(new Callback<RequestData>() {
            @Override
            public void onResponse(Call<RequestData> call, Response<RequestData> response) {
                RequestData requestData = response.body();
                List<Character> characterData = new ArrayList<>();
                if (requestData != null) {
                    for (Character character : requestData.getCharacters()) {
                        character.setName(character.getText());
                        character.setDescription(character.getText());
                        characterData.add(character);
                    }
                }
                characters.setValue(characterData);
            }

            @Override
            public void onFailure(Call<RequestData> call, Throwable t) {
                characters.getValue().clear();
            }
        });
    }

    public MutableLiveData<List<Character>> queryCharacters(final String query) {
        MutableLiveData<List<Character>> queryResults = new MutableLiveData<>();
        List<Character> characterData = new ArrayList<>();
        for (Character character : characters.getValue()){
            if (character.getText().toLowerCase().contains(query.toLowerCase())){
                characterData.add(character);
            }
        }
        queryResults.setValue(characterData);
        return queryResults;
        }



}
