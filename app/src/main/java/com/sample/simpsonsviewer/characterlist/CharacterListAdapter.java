package com.sample.simpsonsviewer.characterlist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sample.simpsonsviewer.R;
import com.sample.simpsonsviewer.characterdetail.CharacterDetailActivity;
import com.sample.simpsonsviewer.characterdetail.CharacterDetailFragment;
import com.sample.simpsonsviewer.pojo.Character;

import java.util.List;

class CharacterListAdapter extends RecyclerView.Adapter<CharacterListAdapter.ViewHolder> {

    private final CharacterListActivity mParentActivity;
    private final List<Character> characters;
    private final boolean mTwoPane;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Character character = (Character) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putParcelable(CharacterDetailFragment.ARG_CHARACTER, character);
                CharacterDetailFragment fragment = new CharacterDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, CharacterDetailActivity.class);
                intent.putExtra(CharacterDetailFragment.ARG_CHARACTER, character);
                context.startActivity(intent);
            }
        }
    };

    CharacterListAdapter(CharacterListActivity parent,
                                  List<Character> items,
                                  boolean twoPane) {
        characters = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Character character = characters.get(position);
        holder.mIdView.setText(character.gettName());
        holder.itemView.setTag(character);
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return characters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;

        ViewHolder(View view) {
            super(view);
            mIdView =  view.findViewById(R.id.id_text);
        }
    }
}

